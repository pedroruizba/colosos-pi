<?php
require '../modelos/bd.php';
date_default_timezone_set('America/Mexico_City');
$date = date('d/m/Y H:i:s', time());
//DEFINIMOS QUE ESTE VA HACER UN ARCHIVO DE TIPO DE EXCEL AL IGUAL QUE SU NOMBRE
header('Content-type: application/vnd.ms-excel;charset=iso-8859-15');
header('Content-Disposition: attachment; filename=report_'.$date.'.xls');
?>
<!-- COLOCAMOS EL CONTENIDO EN HTML Y  EN TABLAS PARA QUE EXCEL NOS RECONOZCA EL CODIGO-->
<table border="1" cellpadding="2" cellspacing="0" width="100%">
    <caption>Reporte generado el <?php echo $date;?> - Por COLOSOS SYSTEM</caption>
    <tr>
        <td style="background-color: green;color:white;">Lugar</td>
        <td style="background-color: green;color:white;">Intensidad</td>
        <td style="background-color: green;color:white;">Temperatura</td>
        <td style="background-color: green;color:white;">Humedad</td>
        <td style="background-color: green;color:white;">Fecha</td>
    </tr>
    <?php
    $contador=0;
    $sql = $conexion->query("SELECT * FROM temblores");//TRAEMOS REGISTROS DE A BASE DE DATOS
	while ($registro = $sql->fetch_assoc())
	{
        $contador++;
        $lugar= $registro['lugar'];
        $medicion= $registro['medicion'];
		$temperatura_c = $registro['temperatura_c'];
        $humedad = $registro['humedad'];
        $fecha = $registro['fecha'];
     //IMPRIMIMOS EL CONTENIDO REGRESADO
    ?>
    <tr>
        <td><?php echo $lugar;?></td>
        <td><?php echo $medicion;?></td>
        <td><?php echo $temperatura_c;?></td>
        <td><?php echo $humedad;?></td>
        <td><?php echo $fecha;?></td>
    </tr>
    <?php
    }
    ?>
     <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Total de temblores: </td>
        <td><?php echo $contador;?></td>
    </tr>
</table>