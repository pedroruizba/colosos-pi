-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 05-11-2018 a las 00:28:55
-- Versión del servidor: 10.2.17-MariaDB
-- Versión de PHP: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `u741841614_col`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `id_invent` int(5) NOT NULL,
  `ubicacion` text COLLATE utf8_spanish_ci NOT NULL,
  `fecha_instalacion` date NOT NULL,
  `ultimo_reporte` datetime NOT NULL,
  `estado` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `id_invent`, `ubicacion`, `fecha_instalacion`, `ultimo_reporte`, `estado`) VALUES
(1, 23432, 'Tecoman', '2018-09-27', '2018-09-27 11:00:00', 0),
(2, 23112, 'Colima', '2018-09-27', '2018-09-27 11:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temblores`
--

CREATE TABLE `temblores` (
  `id` int(11) NOT NULL,
  `lugar` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `n_estacion` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `medicion` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `temperatura_c` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `humedad` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `temblores`
--

INSERT INTO `temblores` (`id`, `lugar`, `n_estacion`, `medicion`, `temperatura_c`, `humedad`, `fecha`) VALUES
(34, 'Tecoman', '1', '3.88\r', '25.00\r', '69.00\r', '2018-11-03 15:19:25'),
(35, 'Tecoman', '1', '9.07\r', '25.00\r', '69.00\r', '2018-11-03 15:19:37'),
(36, 'Tecoman', '1', '7.60\r', '25.00\r', '69.00\r', '2018-11-03 15:25:37'),
(37, 'Tecoman', '1', '8.28\r', '25.00\r', '69.00\r', '2018-11-03 15:26:43'),
(38, 'Tecoman', '1', '4.25\r', '25.00\r', '69.00\r', '2018-11-03 15:26:57'),
(39, 'Tecoman', '1', '4.62\r', '23.00\r', '69.00\r', '2018-11-03 15:44:35'),
(40, 'Tecoman', '1', '3.82\r', '23.00\r', '69.00\r', '2018-11-03 15:51:35'),
(41, 'Tecoman', '1', '5.10\r', '24.00\r', '69.00\r', '2018-11-03 15:56:57'),
(43, 'Tecoman', '1', '5.41\r', '23.00\r', '69.00\r', '2018-11-03 10:02:00'),
(44, 'Tecoman', '1', '8.25\r', '25.00\r', '68.00\r', '2018-11-03 10:06:00'),
(45, 'Tecoman', '1', '7.49\r', '23.00\r', '69.00\r', '2018-11-03 10:08:00'),
(46, 'Tecoman', '1', '6.14\r', '23.00\r', '69.00\r', '2018-11-03 10:09:00'),
(47, 'Tecoman', '1', '8.10\r', '23.00\r', '69.00\r', '2018-11-03 10:13:00'),
(48, 'Tecoman', '1', '4.98\r', '23.00\r', '69.00\r', '2018-11-03 10:14:28'),
(49, 'Tecoman', '1', '4.85\r', '27.00\r', '69.00\r', '2018-11-04 17:45:28'),
(50, 'Tecoman', '1', '4.73\r', '22.00\r', '70.00\r', '2018-11-04 17:46:32'),
(51, 'Tecoman', '1', '4.60\r', '27.00\r', '67.00\r', '2018-11-04 17:48:17'),
(52, 'Tecoman', '1', '14.86\r', '24.00\r', '66.00\r', '2018-11-04 17:49:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` text COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `usuario` text COLLATE utf8_spanish_ci NOT NULL,
  `password` text COLLATE utf8_spanish_ci NOT NULL,
  `perfil` text COLLATE utf8_spanish_ci NOT NULL,
  `foto` text COLLATE utf8_spanish_ci NOT NULL,
  `estado` int(11) NOT NULL,
  `ultimo_login` datetime NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `correo`, `usuario`, `password`, `perfil`, `foto`, `estado`, `ultimo_login`, `fecha`) VALUES
(1, 'Pedro Ruiz', 'pruiz2@ucol.mx', 'pedroruizba', '$2a$07$asxx54ahjppf45sd87a5auxVFBrZzszWfGpQ7whneGIrRH/kChW/m', 'Administrador', 'vistas/img/usuarios/pedroruizba/516.png', 1, '2018-11-03 09:45:12', '2018-11-03 15:45:12'),
(2, 'admin', 'admin@pedroruizba.xyz', 'admin', '$2a$07$asxx54ahjppf45sd87a5auXBm1Vr2M1NV5t/zNQtGHGpS5fFirrbG', 'Administrador', '', 1, '2018-11-04 17:45:03', '2018-11-04 23:45:03'),
(6, 'invitado', 'invitado@colosos.esy.es', 'invitado', '$2a$07$asxx54ahjppf45sd87a5auE65LzaNkVcQwnJ.cV8qQ1oz7B8.AAku', 'usuario', '', 1, '2018-11-01 19:15:09', '2018-11-02 01:15:09'),
(7, '123', '123@gmail.com', '123', '$2a$07$asxx54ahjppf45sd87a5auGZEtGHuyZwm.Ur.FJvWLCql3nmsMbXy', 'usuario', '', 1, '2018-10-23 20:50:58', '2018-10-24 01:50:58');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `temblores`
--
ALTER TABLE `temblores`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `temblores`
--
ALTER TABLE `temblores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
