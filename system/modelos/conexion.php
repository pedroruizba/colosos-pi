<?php
//CREAMOS LA CLASE PARA CONECTARNOS POR PDO
class Conexion{
	static public function conectar(){
		    require 'credentials_conexion.php';
			$link = new PDO("mysql:host=$host;dbname=$base_de_datos",
							"$user",
							"$pass");
		$link->exec("set names utf8");

		return $link;
	}

}