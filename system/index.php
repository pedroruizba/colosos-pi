
<?php
	/*=============================================
	CARGA DE TODOS LOS CONTROLADORES Y MODELOS, ADEMAS DE LA PLANTILLA PARA PODER CARGAR LOS DEMÁS
    =============================================*/	
    
require_once "controladores/plantilla.controlador.php";
require_once "controladores/usuarios.controlador.php";
require_once "controladores/temblores.controlador.php";
require_once "controladores/productos.controlador.php";

require_once "modelos/temblores.modelo.php";
require_once "modelos/productos.modelo.php";
require_once "modelos/usuarios.modelo.php";



$plantilla = new ControladorPlantilla();
$plantilla -> ctrPlantilla();
