<?php
	/*=============================================
  CONTENIDO A MOSTRAR EN LAS CAJAS SUPERIORES DE LOS TEMBLORES AL IGUAL QUE ESTACIONES INSTALADAS
	=============================================*/	

$item = null;
$valor = null;
$orden = "id";
//CONTAMOS TODOS LOS REGISTROS DE CADA TABLA
$temblores = Controladortemblores::ctrMostrartemblores($item, $valor);
$totaltemblores = count($temblores);

$clientes = ControladorUsuarios::ctrMostrarUsuarios($item, $valor);
$totalusuarios = count($clientes);

$productos = ControladorProductos::ctrMostrarProductos($item, $valor, $orden);
$totalProductos = count($productos);

?>



<div class="col-lg-3 col-xs-6">

  <div class="small-box bg-aqua">
    
    <div class="inner">
      
      <h3><?php echo number_format($totaltemblores); ?></h3>

      <p>Temblores este mes</p>
    
    </div>
    
    <div class="icon">
      
      <i class="ion ion-stats-bars"></i>
    
    </div>
    
    <a href="temblores" class="small-box-footer">
      
      Más info <i class="fa fa-arrow-circle-right"></i>
    
    </a>

  </div>

</div>
<!-- LIMITAMOS CONTENIDO A DIFERENTES TIPOS DE USUARIOS POR EJEMPLO INVITADO NO PUEDE VER ESTA SESSIÓN-->
<?php if($_SESSION["perfil"] !="invitado"){?> 
<div class="col-lg-3 col-xs-6">

  <div class="small-box bg-green">
    
    <div class="inner">
    
      <h3><?php echo number_format($totaltemblores); ?></h3>

      <p>Temblores registrados</p>
    
    </div>
    
    <div class="icon">
    
      <i class="ion ion-clipboard"></i>
    
    </div>
    
    <a href="temblores" class="small-box-footer">
      
      Más info <i class="fa fa-arrow-circle-right"></i>
    
    </a>

  </div>

</div>

<div class="col-lg-3 col-xs-6">

<div class="small-box bg-red">

  <div class="inner">
  
    <h3><?php echo number_format($totalProductos); ?></h3>

    <p>Estaciones instaladas</p>
  
  </div>
  
  <div class="icon">
    
    <i class="ion ion-pie-graph"></i>
  
  </div>
  
  <a href="productos" class="small-box-footer">
    
    Más info <i class="fa fa-arrow-circle-right"></i>
  
  </a>

</div>

</div>
<?php }?>
<!-- SOLO EL AMDINISTTRADOR PUEDE VER-->
<?php if($_SESSION["perfil"] =="Administrador"){?>
<div class="col-lg-3 col-xs-6">

  <div class="small-box bg-yellow">
    
    <div class="inner">
    
      <h3><?php echo number_format($totalusuarios); ?></h3>

      <p>Usuarios</p>
  
    </div>
    
    <div class="icon">
    
      <i class="ion ion-person-add"></i>
    
    </div>
    
    <a href="usuarios" class="small-box-footer">

      Más info <i class="fa fa-arrow-circle-right"></i>

    </a>

  </div>

</div>

<?php }?>
