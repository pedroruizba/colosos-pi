<!-- CÓDIGO DE LA TABLA DE LOS TEMBLORES-->
<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Control temblores
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Control temblores</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="box">
    <div class="box-header with-border">
    <?php if($_SESSION["perfil"] =="Administrador" || $_SESSION["perfil"] =="usuario"){?> 
  <button class="btn btn-primary" data-toggle="modal" >
    <a style="color: #ffffff;" href="generate_report/xls.php">
    Descargar reporte completo en xls
</a>
  </button>
  <?php }
  else{
    echo "<center><h4 class='box-title'>Hola! invitado, Tienes funciones limitadas.</h4>";
      echo"<p>Registrate para descargar reportes personalizados.</p></center>";
  }
  
  ?>
  

</div>
      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Lugar</th>
           <th>No. estación</th>
           <th>Medición (Escala)</th>
           <th>Temperatura °c</th>
           <th>Humedad</th>
           <th>Gravedad</th>
           <th>Fecha</th>
         </tr> 

        </thead>

        <tbody>

        <?php

        $item = null;
        $valor = null;

        $temblores = Controladortemblores::ctrMostrartemblores($item, $valor);

       foreach ($temblores as $key => $value){
         
          echo ' <tr>
                  <td>'.($key+1).'</td>
                  <td>'.$value["lugar"].'</td>
                  <td>'.$value["n_estacion"].'</td>
                  <td>'.$value["medicion"].'</td>
                  <td>'.$value["temperatura_c"].'</td>
                  <td>'.$value["humedad"].'</td>';

                  if($value["medicion"] <= "4.9"){

                    echo '<td><button class="btn btn-success btn-xs">Moderado</button></td>';

                  }
                  else{

                    echo '<td><button class="btn btn-danger btn-xs">Fuerte</button></td>';

                  }    

                  echo '<td>'.$value["fecha"].'</td>
                </tr>';
        }


        ?> 

        </tbody>

       </table>

      </div>

    </div>

  </section>

</div>


