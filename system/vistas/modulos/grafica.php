<?php
require("modelos/bd.php");
	/*=============================================
	CODIGO EXCLUSIVAMENTE PARA LA GENERACIÓN DE LA GRAFICA
	=============================================*/	
?>
<!doctype html>
<html>

<head>
	<title>Line Chart</title>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
	
	<style>
		canvas {
			-moz-user-select: none;
			-webkit-user-select: none;
			-ms-user-select: none;
		}
		.chart-container {
			width: 900px;
			margin-left: 40px;
			margin-right: 40px;
		}
		.container {
			display: flex;
			flex-direction: row;
			flex-wrap: wrap;
			justify-content: center;
		}
	</style>
</head>

<body>
	<div class="container">
		<div class="chart-container">
			<canvas id="chart-legend-pointstyle"></canvas>
		</div>
	</div>
	<script>
		var color = Chart.helpers.color;
		function createConfig(colorName) {
			return {
				type: 'line',
				data: {
					labels: [ <?php $sql=$conexion->query("SELECT * FROM temblores");
					while($registro=$sql->fetch_assoc())
					{
						$fecha=$registro['fecha'];
						$fecha=date_create($fecha);
						$fecha=date_format($fecha,'d/m/Y');
						?>
						'<?php echo $fecha; ?>',
					<?php	
					}
					?>],
					datasets: [{
						label: 'Magnitud (Richter)',
						data: [
							<?php $sql=$conexion->query("SELECT * FROM temblores");
					while($registro=$sql->fetch_assoc())
					{
						$medicion=$registro['medicion'];
						echo "$medicion,";
					}
					?>
						],
						backgroundColor: color(window.chartColors[colorName]).alpha(0.5).rgbString(),
						borderColor: window.chartColors[colorName],
						borderWidth: 1,
						pointStyle: 'rectRot',
						pointRadius: 5,
						pointBorderColor: 'rgb(0, 0, 0)'
					}]
				},
				options: {
					responsive: true,
					legend: {
						labels: {
							usePointStyle: false
						}
					},
					scales: {
						xAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'Fecha'
							}
						}],
						yAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'Magnitud'
							}
						}]
					},
					title: {
						display: true,
						text: 'Normal Legend'
					}
				}
			};
		}

		function createPointStyleConfig(colorName) {
			var config = createConfig(colorName);
			config.options.legend.labels.usePointStyle = true;
			config.options.title.text = 'Temblores registrados';
			return config;
		}

		window.onload = function() {
			[ {
				id: 'chart-legend-pointstyle',
				config: createPointStyleConfig('blue')
			}].forEach(function(details) {
				var ctx = document.getElementById(details.id).getContext('2d');
				new Chart(ctx, details.config);
			});
		};
	</script>
</body>

</html>
