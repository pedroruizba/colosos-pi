<!-- CODIGO CORRESPONDIENTE A LA TABLA DE LAS ESTACIONES INSTALADAS -->
<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Control estaciones
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Control estaciones</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="box">
      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Ubicación</th>
           <th>Fecha de instalación</th>
           <th>Último reporte</th>
           <th>Estado</th>
         </tr> 

        </thead>

        <tbody>

        <?php

        $item = null;
        $valor = null;

        $productos = ControladorProductos::ctrMostrarProductos($item, $valor);

       foreach ($productos as $key => $value){
         
          echo ' <tr>
                  <td>'.($key+1).'</td>
                  <td>'.$value["ubicacion"].'</td>
                  <td>'.$value["fecha_instalacion"].'</td>';
                  echo '<td>'.$value["ultimo_reporte"].'</td>';

                  if($value["estado"] == "1"){

                    echo '<td><button class="btn btn-success btn-xs">Activo</button></td>';

                  }
                  else{

                    echo '<td><button class="btn btn-danger btn-xs">Desactivado</button></td>';

                  }    

                  echo '</tr>';
        }


        ?> 

        </tbody>

       </table>

      </div>

    </div>

  </section>

</div>


