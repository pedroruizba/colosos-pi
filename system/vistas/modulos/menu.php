<!--=============================================
MENÚ PARA TODO USUARIO, LIMITANDO CIERTAS SECCIONES A DIFERENTES USUARIOS 
=============================================-->
<aside class="main-sidebar">

	 <section class="sidebar">

		<ul class="sidebar-menu">

		<li class="active">

				<a href="inicio">

					<i class="fa fa-home"></i>
					<span>Inicio</span>

				</a>

			</li>

			<li>
		<?php

		if($_SESSION["perfil"] == "Administrador"){

			echo '

				<a href="usuarios">

					<i class="fa fa-user"></i>
					<span>Usuarios</span>

				</a>

			</li>';

		}

		?>
	<li>
		<a href="temblores">
		<i class="fa fa-calendar"></i>
			<span>Registro de temblores</span>
		</a>
	</li>
	<?php
		if($_SESSION["perfil"] != "invitado"){
	?>
	<li>
		<a href="productos">
		<i class="fa fa-bullseye"></i>
			<span>Estaciones instaladas</span>
		</a>
	</li>
	<?php
		}
	?>

		</ul>

	 </section>

</aside>