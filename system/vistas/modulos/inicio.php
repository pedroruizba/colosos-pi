<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Tablero
      
      <small>Panel de Control</small>
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Tablero</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="row">
      
    <?php

    

      include "inicio/cajas-superiores.php";

    ?>

    </div> 

     <div class="row">
       
        <div class="col-lg-12">

          <?php

          if($_SESSION["perfil"] =="Administrador"){
          
           //include "reportes/grafico-ventas.php";

          }

          ?>

        </div>

        <div class="col-lg-6">

          <?php

          if($_SESSION["perfil"] =="Administrador"){
          
           //include "reportes/productos-mas-vendidos.php";

         }

          ?>

        </div>

         <div class="col-lg-6">

          <?php

          if($_SESSION["perfil"] =="Administrador"){
          
           //include "inicio/productos-recientes.php";

         }

          ?>

        </div>

         <div class="col-lg-12">
           
          <?php

          if($_SESSION["perfil"] =="usuario"){

             echo '<div class="box box-success">

             <div class="box-header">

             <h1>Bienvenid@ ' .$_SESSION["nombre"].'</h1>

             </div>

             </div>';

          }

          ?>
          
          <!-- charts-->
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>
              <hr>
              <?php 
              if($_SESSION["perfil"] == "invitado"){
                  echo "<center><h4 class='box-title'>Hola! invitado, Tienes funciones limitadas.</h4>";
                  echo"<p>Registrate para obtener información de un lugar en especifico</p></center>";
              }
                ?>
              <br>
              <h3 class="box-title">GRAFICA AL MOMENTO</h3>

             
            </div>
            <div class="box-body">
              <!--<div id="interactive" style="height: 300px; padding: 0px; position: relative;"><canvas class="flot-base" width="270" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 270px; height: 300px;"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px;"><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 97px; top: 283px; left: 21px; text-align: center;">0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 97px; top: 283px; left: 66px; text-align: center;">20</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 97px; top: 283px; left: 114px; text-align: center;">40</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 97px; top: 283px; left: 162px; text-align: center;">60</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 97px; top: 283px; left: 210px; text-align: center;">80</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px;"><div class="flot-tick-label tickLabel" style="position: absolute; top: 270px; left: 13px; text-align: right;">0</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 216px; left: 7px; text-align: right;">20</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 162px; left: 7px; text-align: right;">40</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 108px; left: 7px; text-align: right;">60</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 54px; left: 7px; text-align: right;">80</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 0px; left: 1px; text-align: right;">100</div></div></div><canvas class="flot-overlay" width="270" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 270px; height: 300px;"></canvas></div> -->
             <?php
              include("grafica.php");
             ?>
            </div>
            <!-- /.box-body-->
          </div>
          <!--charts final-->

         </div>

     </div>

  </section>
 
</div>
